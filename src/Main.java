
public class Main {

	public static void main(String[] args) {
		Library lib = new Library();
		Student student = new Student("Earth","5610450161");	//Student info
		Student student2 = new Student("Panupong","5610451205");
		Teacher teacher1 = new Teacher("A.AAA","D14-1");
		Staff staff1 = new Staff("John","1234");
		Book book1 = new Book("Big java","2012");	//Book 1
		Book book2 = new Book("PPL","2014");	//Book 2
		
		//Reference book cannot be borrowed.
		ReferencesBook refBook1 = new ReferencesBook("Java Reference", "2012"); //Author, Book's name ects
		lib.addBook(book1);
		lib.addBook(book2);
		lib.addRefBook(refBook1);
		
		System.out.println("All books in Lib: "+lib.getBookCount());
		
		//System.out.println(lib.getBookCount());	//get number of book available ..3
		lib.borrow(student,book2);
		System.out.println("Book Available : "+lib.getBookCount()); //..2
		lib.returnBook(student,book2);
		System.out.println("Book Available : "+lib.getBookCount()); //..3
		lib.borrow(student,refBook1);
		System.out.println("Book Available : "+lib.getBookCount()); // 3
		
		lib.borrow(teacher1,book1);
		System.out.println("Book Available : "+lib.getBookCount()); // 2
		lib.borrow(staff1, book2);
		System.out.println("Book Available : "+lib.getBookCount()); // 1
		
		System.out.println(student.getName());
		System.out.println(lib.getBorrowers());
		//How to Dictionary
		/*
		 HashMap hm =new HashMap();
		 hm.put("5510406666", new ArrayList<String>());
		 ((ArrayList<String>) hm.get("5510406666")).add("Big Java");
		 ((ArrayList<String>) hm.get("5510406666")).add("PPL");
		 for(String s : ((ArrayList<String>) hm.get("5510406666"))){
		 	System.out.println(s);
		 	
		 	if("Big Java".equals(s)){
		 		Sytstem.out.println("Big java found");
		 		}
		 	else{
		 		System.out.println("Big java not found");
		 		}
		 	}
		 */
		
	}

}
